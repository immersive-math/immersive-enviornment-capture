# IMRE Lab Hardware and Software

## Software

- Unity 3D

## Hardware Maintenance Tasks

### Inventory System

### Windows Software

### Linux Software
Our Linux machines currently use Pacman for package managenet.
Run ```sudo pacman -Syu``` to upgrade all packages.  Install any software using `add or remove software` and the package management system will handle all software updates.

### Docker Containers
Archimedes runs a few docker containers for the lab. Manage these with Portainer. GUI in the web broweser at `localhost:9000`.

## Hardware Maintenance Schedule

### Weekly Maintance

### Montly Maintenance
### Annual Maintenance (Beginning of June)
