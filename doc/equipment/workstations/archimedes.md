
## Hardware Configuration

- Ryzen 5th Gen R7

- NVidia GTX 1080

- 32 GB RAM

## Software Configuration

### PopOS! NVidia Edition

We use PopOS! NVidia edition for their stable support of our NVidia GPU. If we upgrade this GPU in the future, strongly consider using an AMD GPU.

- configured with recovery partiion (8GB)

- configured with 32 GB of swap

- configured with luks encryption (luks type 2)

## Users:

- IMRE Lab (for video editing/graphics/etc)

- IMRE Dev (for linux development)

- External User (for external users)

### ZeroTier Network

[ZeroTier Central](https://my.zerotier.com)

### BTRFS

We use the BTRFS File System. This lets us cleanly merge drives, those it does not necessairly give the redundancy of RAID. Any files stored on Archimedes directly should understand that the disks are scratch disks and they may be lost.

### BTRBK Snapshots

<<TODO>>

## AntiVirus

[Software - Information Technologies](https://umservices.umaine.edu/software/antivirus/index.cgi)

We use System Center Endpoint Protection, consistent with UMS IT policy

### Applications & Sources

#### General Use:

- caffeine

- Zotero

- marktext (flathub)

- syncthing (manage on port 8384)

```bash
sudo curl -s -o /usr/share/keyrings/syncthing-archive-keyring.gpg https://syncthing.net/release-key.gpg
sudo echo "deb [signed-by=/usr/share/keyrings/syncthing-archive-keyring.gpg] https://apt.syncthing.net/ syncthing stable" | sudo tee /etc/apt/sources.list.d/syncthing.list
sudo apt-get update
sudo apt-get install syncthing
```

- syncthing extension for Gnome

- Cafiene indicator extension for Gnome

#### Video Editing and Graphics:

- kdenlive

- inkscape

- gimp

- obs-studio

- vlc

- ffmpeg

- darktable

#### Development:

- steam

- gamehub

- zsh

- git

- Jetbrains Rider (flathub)

- Jetbrains Pycharm (flathub)

- jetbrains Webstorm (flathub)

- UnityHub (flathub)

#### Communication

- Discord

- Zoom

### Manually Intsalled Libaries/etc

- r-base

- anaconda

- docker.io

- qrencode

- wget

- curl

- libavcodec-extra

- libdvd-pkg

```bash
sudo apt install libdvd-pkg
sudo dpkg-reconfigure libdvd-pkg
```

- Flatseal (flathub)

- libgl1-mesa-glx

- libegl1-mesa

- libxrandr2

- libxrandr2

- libxss1

- libxcursor1

- libxcomposite1

- libasound2

- libxi6

- libxtst6

- Latex Packages

```
texlive texlive-font-utils texlive-pstricks-doc texlive-base texlive-formats-extra texlive-lang-german texlive-metapost texlive-publishers texlive-bibtex-extra texlive-latex-base texlive-metapost-doc texlive-publishers-doc texlive-binaries texlive-latex-base-doc texlive-science texlive-extra-utils texlive-latex-extra texlive-science-doc texlive-fonts-extra texlive-latex-extra-doc texlive-pictures texlive-xetex texlive-fonts-extra-doc texlive-latex-recommended texlive-pictures-doc texlive-fonts-recommended texlive-humanities texlive-lang-english texlive-latex-recommended-doc texlive-fonts-recommended-doc texlive-humanities-doc texlive-luatex texlive-pstricks perl-tk
```