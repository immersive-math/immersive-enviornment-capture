# Brahmagupta
Brahmagupta and Cantor are identical setups for Wireless Vive Pro and Vive Pro use on Windows 11.

## Hardware

- CPU: [Ryzen 1800X](https://www.amd.com/en/products/cpu/amd-ryzen-7-1800x) (8 cores, 16 threads, 95W TDP)

- GPU: [XFX Swift 319 - AMD/Radeon RX 6900XT](https://www.xfxforce.com/shop/xfx-speedster-swft-319-amd-radeon-tm-rx-6900-xt-core) (16 GB VRAM)

- RAM: [2x8 GB GSkill Ripjaws V](https://www.gskill.com/product/165/184/1536047367/F4-3200C14D-16GVR) (3200 MHz)

- Motherboard: [ASUSTeK PRIME X370-Pro](https://www.asus.com/us/motherboards-components/motherboards/prime/prime-x370-pro/)

- HMD: [Vive Pro](https://www.vive.com/us/product/vive-pro/) (6DOF, 90 Hz])

- Controllers: [Valve Index Controllers](https://www.valvesoftware.com/en/index/controllers) (6DOF)

- [Wireless Vive Adapter](https://www.vive.com/us/accessory/wireless-adapter/)

## User Accounts

- IMRE LAB (for deployment, recording)

- IMRE DEV (for software development)

- External User

## Software

- Firefox (set default away from edge)

- OBS-Studio

- [AMD Radeon Software](https://www.amd.com/en/technologies/software)

- Libre Office

- Syncthing

- Steam

- Vive Wireless

- SteamVR

- Unity Hub

- Jetbrains Rider

- Jetbrains WebStorm

- Discord

- Git

- Zoom

- ZeroTier

## Standardized Desktop Settings

- No Desktop Icons/Files (group policy)

## AntiVirus

[Software - Information Technologies](https://umservices.umaine.edu/software/antivirus/index.cgi)

We use System Center Endpoint Protection, consistent with UMS IT policy


## Adjacent Classroom Monitor
This is attached as a third monitor on Brahmagupta. Mirrored to TV.

## Configuration for Poster Printing (CANNON IMAGEGRAPH)
[Canon Software](https://www.usa.canon.com/support/p/imageprograf-gp-300#idReference%3Dsoftware-drivers)
 - imagePROGRAF GP-300 Full Driver & Software Package (Windows) 
 - Direct Print Plus

Hostname: `canongp300.umeedu.maine.edu`

## Steam Library
Steam library is mapped to `Z:/steam-library`. This is a SMB share from the Poincare TrueNAS Server.

This needs to be configured on the IMRE-LAB and IMRE-DEVELOPER accounts.

## OBS Recordings
OBS recordings are mapped to `Y:/Brahmagupta`. This is a SMB share from the Poincare TrueNAS Server.

This needs to be configured on the IMRE-LAB and IMRE-DEVELOPER accounts.

### OBS Configuration
OBS Settings are saved to `Y:/OBS_Configuration`. This is a SMB share from the Poincare TrueNAS Server.

This needs to be configured on the IMRE-LAB and IMRE-DEVELOPER accounts.