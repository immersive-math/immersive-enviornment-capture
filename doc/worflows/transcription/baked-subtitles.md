# Baked Subtitles with FFMPEG

Files:
 - Video file (video.mp4 or video.mkv)
 - Subtitle track (subtitles.srt)

```bash
ffmpeg -i video.mkv -vf subtitles=subtitles.srt video_bakedSubtitles.mkv
```

For naming convention, add `_bakedSubtitles` to the end of the file name.
