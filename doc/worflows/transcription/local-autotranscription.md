## Local Transcription with Whisper

## Setup:
1. Install ffmpeg
```bash
apt install ffmpeg
```

2. Install yt-whisper
```bash
pip install git+https://github.com/m1guelpf/yt-whisper.git
```

3. Clone whisper-subtitles

```bash
git@gitlab.com:camden-bock/whisper-subtitles.git
```

## Run Whisper

In the current version Whisper, only one file can be processed at a time. Batched runs do not produce correct results.

In the whisper-subtitles directory,
1. Edit `main.py` so that `audio = 'path/to/filename.ogg'` assigns the correct path to the .ogg file prepared for transcription.
2. Run 'main.py'
```bash
python main.py
```
3. A `srt` file will be produced with the same name and path as the audio file. This includes verbal transcription and timestamps.
