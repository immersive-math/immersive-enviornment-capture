# Embedded Subtitles with FFMPEG

Files:
 - Video file (video.mp4 or video.mkv)
 - Subtitle track (subtitles.srt)

```
ffmpeg -i video.mkv -c copy -newsubtitle subtitles.srt video_embeddedsubtitles.mkv
```
