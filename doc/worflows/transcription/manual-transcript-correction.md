# KDE Subtitle Composer

As a convention, save manually edited subtitles in a file with name ending in `_corrected.srt`.

## Setup
[Install KDE Subtilte Composer](https://subtitlecomposer.kde.org/)

```bash
apt install subtilte-composer
```

## Basic Operation
 - `f2` to edit selected subtitle

## Speaker Identification
 - When speakers are seperated into L/R channels, the waveform on the right hand side of Subtitle Composer can guide speaker identification.
  - Multi-track audio is supported, select the appropriate track for the isolated speaker's audio.

## Timings
 - Split subtitle sections as needed to breakup long sections or cross-talk. 
 - Use the waveform on the right hand side of Subtilte Composer to adjust timings as necessary.
 - When transcribing verbal dialoug, leave empty space for silence.

## Conventions
 - Identify interviewers by their last inital, `B:`
 - Identify participants by thier location in the view `PR:` for the participant with the right feed. Participants will be represented with pseudonames when refacotred.
 - Denote long pauses with `.`. Long pauses are also measured by distance between consequative subtitlecomposer
 - Denote short pauses with `,`.
 - Denote interruptions with `--`.
 - Describe non-verbal elements in brackets `[]`

## Refactoring with Python
TODO:
 - Link repository
 - Bold speaker Identification
 - Color by speaker
 - Format verbal/non-verbal
