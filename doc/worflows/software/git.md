# Git

Git is the center of all of our software repositories.  Git describes the repository based on the *changes* made in a series of *commits*.
Every time that work is completed, a *commit* is made recording the file changes and a description of what work was done. In sophisticated implementations, these comments on commits can be linked to *issues* in kanban worflows.

Git can be used from the command line or from a GUI client.  Often, we use git to connect to GitHub and Git Lab.

Git functions around four actions.

### Stage & Commit
Stage selects the files to be commited, so that one work session can be broken into multiple work tasks when appropriate.
Commit links a comment to the changes files.

For example, the following command will stage and commit all files 

```
git add -A
git commit -a -m "some work was done here"
``` 

To temporarily use a different author:
```
git commit -a -m "some work was done here" --author="IMRE LAB <iimre@maine.edu>"
```

### Pull
Pull downloads the latests commits (on the checked out branch) to the local respistory.  The pull command will *merge* commits from simultaneulous work by multiple developers or multiple sessions when possible.
Use 
```
git pull
```
to download the most recent changes to your local device. When multiple users simultaneously work on the same device this will need to be done each time you are ready to commit.  Otherwise, run ```git pull``` at the beginning of your session.

### Push
Push does the opposite of pull.  Push sends the local changes on your local branch to the remote (e.g. GitLab).  After pushing, your changes will be avaialble to all other sessions. For open-source repositories, after a push your changes and commnets on commits are public.

Use ```git push``` to send your commit to the remote server at the end of a session. If your local branch is not up to date, you will need to 
 run the following sequence.

```
git  pull
git commit -A -m 
git push
```

If your changes can be automatically merged, this sequence will integrate your changes and upload them to the server.  If not, use a program (e.g. GitKraken, Merge) to resolve any conflicts.

### Pull Requests
A pull requests moves changes from one branch into another.  Never manually merge branches locally, always use the utility in GitLab or GitHub.
Pull requests should document the code changes (e.g. bug fix or feature implementation), confirm that the code includes any necessary documentation changes, and explain how the code has been tested to ensure its inegrity.

### Git Ignore
A file named ```.gitignore``` will list all of  the directoreis and file extensions that should be ignored by git.  It is generally important to ignore temporary files and binary files.  When binary files must be included, use GitLFS.

## Git Flow

Git Flow is a structure for working with Git repositories. In particular, Git Flow structures what type of work is conducted on which branch to keep repositories organized and stable while developing multiple modular features.

 - Main: The main branch has the core code that supports production environments.  Everything on the main branch has been extensively tested, including integrations of modular parts. Builds are constructed from the main branch (either with CI/CD or manually in Unity).  Any user should be able to download the main branch and have a functional (if old) copy of the software.
 - Develop: The develop branch runs parallel to main.  Tested features are pulled into develop, where their intergration and interactions with other features are extensively tested.  When stable, a pull-request is made to main.
 - Feature:  Individual features are developed on branches with a name of the form ```feature/FEATURENAME```.  These branches isolate work on particular features, branching off of *develop*.  Pulll requests may come from develop when significant changes will change how the new feature is supported.  Otherwise, the feature is preprepared and tested on feature and when stable a pulle request is made to develop. Merge-commits from these pull requests are *squashed*.
 - Hotfix: hotfixes are not intended to be used, but sometimes inevitable.  (For example externally driven changes in driver support).  Either the ```main```or ```develop``` may be hotfixed to urgently debug an issue in a production or near-production versino of the environment.

Our most recent work, in complex projects, is intended to use GitFlow.  Historically, we have moved through multiple branching structures before settling here. The branch history of HandWaver is especially complicated.
