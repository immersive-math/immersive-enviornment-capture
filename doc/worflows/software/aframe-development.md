# Development for A-Frame with NPM and Webstorm

## Working with A-Frame Projects

Beginning in 2019, we experimented with A-Frame as an alternative to targeting different hardware configurations in Unity.

Our work with A-Frame started in GitHub but was quickly migrated to  GitLab. All work should be available on GitLab.

Our a-frame projects remain 100% open source (all dependencies are open source). All dependencies are linked through NPM.  If you are working locally, after cloning the repository, ```npm ci``` will install all dependencies and ```npm start``` will start a local web server to debug content.  The combination of the use of NPM and the platform agnostic approach of a-frame should prevent most version conflict issues that were present in the Unity Workflow.

Because all dependencies are open-source and npm is used to manage packages, CI/CD integrations have been set up for A-Frame projects.  GitLab CI/CD automatically builds the ```main```branch of the repository for producition versions.  These can be accessed online with a web browser without any knowledge of the software.


## NPM

The node package manager handles dependencies for our A-Frame projects.

`npm ci` will  install any dependencies for an node-based project.  `npm start` will start a web server for debugging.

`npm install -save` will add a dependency to the project.

NPM supported repositories can be browsed here. https://www.npmjs.com/
They are often open-source, but licenses should be checked before integration.

Edit the `package.json` file to make manual changes to npm dependencies.
