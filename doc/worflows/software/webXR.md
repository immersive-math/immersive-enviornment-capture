# Working with CindyJS & WebXR Projects

CindyJS extensions using WebXR paralleled A-Frame development - in particular using the CindyXR module. 
Similar to A-Frame, CindyXR uses WebXR and NPM to have a simple, platform agnostic approacch to XR content.

Note that CindyJS's source is manually referenced in the scripts (not using npm).

Similar to A-Frame use ```npm ci``` and ```npm start``` to start a project locally, or work with production environments from GitLab CI/CD.