# GitLab

## Code Analysis

Gitlab offers built-in code analysis.  https://docs.gitlab.com/ee/user/project/merge_requests/code_quality.html

There are three purposes of code analysis:

 - to enforce code style
 - to identify repeated chunks of code that can be abstracted or (if enough) moved into npm dependencies
 - to identify depreciated components

Code analysis is an automated process that occurs after each commit. On the main branch, a the latest commit with give the overall repository a grade. For each pull request, a code analysis is performed to determine if the code changes introduce new style or efficiency problems into the recieving branch. 

## CI/CD: Automated Builds

