# Short History of IMRE Lab Software Development

Historically, IMRE Software Development started with Unity 3D. This software was a mix of C# code, third-party unity assets, 3D models and other libraries. These repositories were locally hosted, migrated to Bitbucket, migrated to GitHub and finally migrated to GitLab.  Unfortunately, some of the old versions on BitBucket are no longer available as our license has expired.

More recent projects have focused on AFrame.  AFrame is a HTML5 framework for OpenXR, where the software is cross-platform and can be run in the browser.
These repositories started in GitHub and have been migrated to GitLab.  All versions should be accessible in GitLab.

# Short History of IMRE Software Design

Early development at the IMRE Lab was focused on rapidly prototyping new features and integrating those features to piecemeal build a grand vision for spatial dynamic geometry.
As this work progressed, this work was refocused on sprints, targeting smaller intergated feature sets.
Following that work, we attempted to seperate out a platform that multiple environment could use as a core. Finally, we shifted our focus to an open-source and modular design.

In this design, user interaction systems are seperated from a geometric kernel.
Features are seperated out by namespaces, which were designed to be interoperable but further and further seperated.
This would allow for scenes to be built which shared code acrosss projects but only required the code required to support that environment.

HandWaver was initially a platform for building multiple environments, and our most recent work began the process of seperating out modular components.
However, the HandWaver repository would hold all of the code across namespaces, and individual scenes would be constructed to design individual environments. This lead to a Git branching structure where multiple production environments, focused around their own scenes, existed on seperate branches of one main code base.
The shared code would be pulled upsteam and downstream to provide stability and new features to each of the derivative environments.
This process leaves HandWaver fragmented, with features in different states on different branches and only some combinations supporting full integration.
However, the design hoped to allow the geometry kernel to be eventually replaced with a more robust one.

More recent work with A-Frame has seperated different projects into their own repositories.
Dependencies are easier to manage with NPM, so large constructs like an eventual geometry kernel would be designed into an NPM package.  Wherever possible, Open-source NPM packages are used to provide support for User-Interactions or other features sets that are used across scenes.
Consistent approaches to use other community-supported packages for UI have been perfered over in-house solutions for user interactions.
The hope of this solution is to allow others to use and iterate on the software with ease, minimize maintenance and minimize development time when community efforts provide the needed features.

## Open Source, Dependencies

In the most recent work on all software projects, making our code as readable and easy to build as possible has been the focus - in a larger effort to contibute to the open source community.  This has lead to a requirement that dependencies must serve a clear process, be modular whenever possible and always be open source.
