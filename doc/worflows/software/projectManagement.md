# Kanban
The IMRE Lab uses Kanban for project management, using a kanban board on GitHub or GitLab.  Old (no longer accessible) kanban boards were hosted on BitBucket.

To manage a software or analysis project with kanban, projects are divided up into components called issues. 
 An issue should have a clearly defined completion point (if it doesn't break it into components that do) and can be made up of subtasks.  Subtasks can be given checkmarks (using markdown) to track their progress within an issue.
An appropriate issue size usually takes between 1/2 of a day to 10 days to complete.  A few related issues are generally easier to manage than one larger issue.

Issues can come in multiple forms. For software:

 - A bug is a problem with the software, the issue is completed when the software's related functionality is restored.
 - A feature is a new component of code that adds functionality to the project.
 - A scene is an intergrated collection of software components with a particular use case.
 - An idea is a concept that needs to be developed further in design before it can be implemented.

Issues can be linked (e.g. one issue may be a prerequsite for another). 
This structure helps to break larger projects (e.g. a complex scene) into manageable components, and to show how multiple components (possibly from seperate projects) are related.

Large issues might get their own *branches* in gitflow, but each issue should be addressed in a *commit* message when multiple issues are tackled in one branch. A *pull request* should name the related issues and their states.
