# Working Unity 3D Projects
Within a GitHub archive, you will find *releases* which include zipped executibles for versions of the project.  You will also find source code in varying stages of completeness.
The executible files can be run by replicating the environment that the binary was produced for.
Typically, the requirements are as follows:

 - SteamVR
 - HTC Vive
 - Leap Motion Sensor
 - Leap Motion Drivers (typically v4 unless noted otherwise)
 - Windows 10
 - NVidia graphics, NVidia graphics drivers
 - MixCast Mixed Reality Driver
 - 1080p Webcam

At times, Windows updates, SteamVR drivers, NVidia drivers, or LeapMotion drivers have made old executibles incompatable.  It is often difficult to restore the state of the old system, as third party dependencies often either have dependencies of their own or are not possible to download.

## For Unity Environments with incompatible executibles

If you wish to run an old environment where the executible does not exist or it is no longer compatible with recent drivers from Windows and Steam, you will need to build an executible from the source.  This section assumes that you have a section (elsewhere) that explains how to access repositories through Git.

Once you have cloned the repository,  open the repository in the most recent version of Unity3D.
Unity updates frequently to remain compataible with the latest drivers from Windows, NVidia, AMD and SteamVR and often adds or optimizes its own feature set.  When importing an old project, there will be some errors.

### Assets
First, you will be notified that assets or libraries are missing.
 The relevant respotiroy will have a file called dependencies.md (anything in our GitHub repository has these files). 
That file will link to the packages that need to be imported into Unity. 
Follow the links in this file to find the versions of each asset that were used when the origional executible was developed.
Download each package and import it to Unity.
It will take Unity some time to import each package, and one imported package may allow Unity to 'see' references to other missing packages.

[[Graphic of importing unity package]]

Some packages will be broken by earlier versions of Unity.
Make sure that you are using the latest version of the relevant IMRE project, around 2018-2020 we attempted to remove as many non-free assets and dependencies as possible and overall decreased the number of free dependencies.

For open-source projects, you may find a newer version of a package within the linked 'release' page, typically on GitHub.

### Unity & API Versions
Changes to features or APIs from Unity or from dependencies will create more compile errors in the project.
These errors will be revealed in batches as Unity attempts to compile in the console.
Typically, Unity (and the most recent dependencies) do not entirely remove features, but instead references to the features get renamed.
Search for Unity's documentation online (or C# documentation for system-level changes) and most compile errors will be solved with rename and replace.

### Test with new hardware

If you have chagned the hardware that you wish to use (e.g. OSVR, Oculus Quest or LeapMotion controls), be sure to test on the hardware you intend to use.
Our most recent work in Unity began a compatability layer to integrate multiple hardware platforms - in some applications, this will make compatability easy, but cannot be relied up entirely.
If you are using an OSVR compatible device you should be able to remap any controls, as needed. These are typically in seperate components (scripts) than the functinos that are triggered to allow for modular control systems. This is described in more depth in the section on Software Design

### Build an executible
The last step in restoring an old enviornment is to build an executible. Use Unity's build menu to accomplish this, targeting the correct platofrm.
Build for multiple platforms when possible, and zip multiple platform's builds together to be archived.
