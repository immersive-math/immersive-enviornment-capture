# Scanning Books

Scanning of public dommain materials for classroom/educational use only.

## Scan on Flatbed

Configuration:
 - use 300-600 dpi
 - consistent alignment
 - reduce page curl/shadow
 - no OCR at this stage
 - export multi-page PDF

## Merge Flatbed Scans with [PDFTK](https://linux.die.net/man/1/pdftk)

### Setup
On Ubuntu/PopOS, install with:
```bash
apt install pdftk
```

Emailed scans from Canon copiers will be diveded into chunks. Merge these with [PDFTK](https://linux.die.net/man/1/pdftk).

### Operation
```bash
pdftk part1=PART1FILE.PDF part2=PART2FILE.PDF part3=PART3FILE.PDF cat part1 part2 part3 output SCANNED_BOOK.PDF
```

## Use [BRISS 2.0](https://github.com/mbaeuerle/Briss-2.0/tree/v2.0-alpha-3) to crop and split pages.

Download the latest release of Briss 2.0 from GitHub. Extract into your working directory. Also need Java 11 or newer.
Camden Bock has a pull request for Wayland  support [#55](https://github.com/mbaeuerle/Briss-2.0/pull/55).

### Setup
```bash
apt install openjdk-19-jre openjdk-19-jdk openjfx
```
### Operation
Then run briss.

```bash
./Briss02.0/bin/Briss-2.0 -s SCANNED_BOOK.pdf -d SCANNED_BOOK_splitPages.pdf --split-col
```

## OCR, Clean, Optimize and Deskew with Tesseract

### Setup:
```bash
apt install tesseract-ocr ocrmypdf
```

### Operation
```bash
ocrmypdf CHAPTER_FILE.pdf CHAPTER_FILE_ocr.pdf --output-type pdfa --pdfa-image-compression jpeg --deskew --clean-final --optimize 2
```

## Use PDFTK to split into Chapters

For each chapter to export, run PDFTK and select the desired pages. 

```bash
pdftk SCANNED_BOOK_splitPages.pdf cat 1 5-20 SCANNED_BOOK_CHAPTER1.pdf
```
