## Noise reduction in Audacity

1. Open a track from a single source in Audacity (from an .ogg file).
2. Highlight a region with background noise (i.e., air conditioner) but no speech.
3. Select [Effects --> Noise Reduction](https://manual.audacityteam.org/man/noise_reduction.html). In the dialoug, choose `Get noise profile`.
4. Select the region to the track where that background noies occurs.
5. Select [Effects --> Noise Reduction](https://manual.audacityteam.org/man/noise_reduction.html). In the dialoug, press `OK` or `Apply`. The reduction and sensitivity can be tuned in this dialoug. Speaker isolation is a better practice for extended periods of silence. Overuse of noise reduction can distort speech.
