## Isolating Speaker Audio in Audacity

1. Open the audio track (.ogg file) in Audacity.
2. (Optional) For microphones picking up an entire room, synchronzie additional sources as needed. Use the clacker spike to synchronize multiple tracks.
3. Select the [(*) Multi-Tool](https://manual.audacityteam.org/man/multi_tool.html).
4. Highlight non speaking time by selecting the timeline. Hold shift to select across multiple tracks
5. Press `space` to playback the region, check that there is no audio from the intended speaker.
6. Press `ctrl`+`l` to mute the region.
7. Repeat 3-6 for the duration of the audio track.
8. Export speaker audio as an (.ogg) file. For convention, add `_SPEAKERNAME.ogg` to the file name.
9. Repeat 1-8 for each speaker.
