# OBS Configuration

## Settings

### Output - Advanced Mode, Recording

 - Type: `Standard`
 - Path: `Y:/$DeviceName` (network map)
 - Generate Filename without space `true`
 - Recording Format .mkv (this avoids data loss if the recording is interrupted by system failure)
 - Audio Tracks (select at least 1-4)
 - Encoder: `AMD HW H.265 HEVC`
 - Rescale Output: `false`
 
 ### Output - Advanced Mode, Audio
For each track,
 - Bitrate: `256`

### Audio
 - Global Audio Devices: Disable All

### Video: For Dual Capture Mode:
 - Base Canvas Resolution: `3840x1080`
 - Output Resolution: `3840x1080`
 - FPS: `60`

## Scene

### Webcam
 - Device, Brahmagupta: `Polycom Video`
 - Device, Cantor: `Logitech C930e`
 - Resolution/FPS Type: `Custom`
 - Resolution: `1920x1080`
 - FPS: `Match Output`
 - Video Format: `Any`
 - Use Hardware Decoding When Available: `true`
 - Audio Output Mode: `Capture Audio Only`
 - [Optional] Filter: `Chroma-Key, Custom Color`

 ### VR Monitor
 Duplicate this device if capturing Mixed Reality. Requires a second PCIe device.
  - Device: `Elgato Game Capture HD60 Pro`
  - Resolution/FPS: `Device Default`
  - Audio Output Mode: `Capture Audio Only`

 ### VR Mic
 We use an Antlion Microphone Magnetically mounted to the hedaset, rather than the Vive's built-in slot mic.
  - Device: `Antlion USB Adapter`
  - [Optional] Filter: `Noise Reduction`

 ### Room Mic
  - Device, Brahmagupta: `Polycom Video`
  - Device, Cantor: `Logitech C930e`
  - [Optional] Filter: `Noise Reduction`

## Advanced Audio Settings
  - For all sources, Audio Monitoring: `Monitor Off`
  - Adjust Volume as needed for environment.
  - Room Mic, Mono: `false`
  - VR Mic, Mono: `true`
  - Track 1: `Room Mic`
  - Track 2: `VR Mic`
  - Track 3: `Desktop Audio`
  - Tracks 4-6: `Additional Sources, As Needed`
