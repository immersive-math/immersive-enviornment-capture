# A-Painter with Math Diagrams

Key features to attend to in A-Painter include:
 - [ ] drawing in space
 - [ ] drawing with multiple people/multiple hands
 - [ ] manipulating verticies where appropriate.
 - [ ] interacting with primitive avatars.

Triangle scene:
 - [ ] what happens to the area and perimeter when you move the vertices of the triangle?
 - [ ] can you slice the triangle so that the slice isn't changed?
 - [ ] try teleporting while holding a vertex.

Pyramid scene:
 - [ ] what happens to the volume of the pyramid when you move the apex?
 - [ ] what happens to the surface area of the pyramid when you move the apex?
 - [ ] can you slice the pyramid so that the slice isn't changed when you move the apex?
 - [ ] try teleporting while holding a vertex.

 Cube Scene:
  - [ ] how is the volume of a square-based pyramid related to the volume of a cube?
