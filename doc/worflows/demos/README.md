# New participant orientation

## Preparing for participants using VR.

0. If necessary start Wireless VR, Steam, SteamVR
1. Confirm that the headset is tracking
- [ ] Complete room setup if necessary
2. Load the enviornment
- [ ] For WebXR enviornmetns (including A-Painter), open the enviornment in firefox from its URL AFTER the headset is fully setup. Then press the Enter VR button. IF controllers are poorly aligned, refresh the browser.
- [ ] For app environments, open from Steam or Oculus menu system.

## Script for introducing a participant to VR (non-research settings):

0. Make introductions

1. Talk about how you will be seeing and walking around a 3D virtual world, through a headset that track how your head moves through space.
- [ ] remind participants that they may remove the headset or ask for assistance at any time if they are uncomfortable for any reason.

2. Describe the boundary system and how to navigate the space
- [ ] for the Vive, the boundary is a blue fence marking walls/etc.
- [ ] for the Quest, the boundary is a fence that morphs into the camera's vision of the outside world

3. Describe how to navigate around other people
- [ ] with synchronized physical/virtual spaces - their avatar represents their physical person
- [ ] with non-synchronized spaces - the avatar is not in the same location as the physical person, stay inside boundaries in split rooms.

4. Describe the controller mapping. This may differ for different controllers.
- [ ] Trigger
- [ ] Touchpad/Joystick
- [ ] Grip
- [ ] Primary Button
- [ ] Secondary button

<img src="https://docs.unity3d.com/2018.4/Documentation/uploads/Main/vive_controllers.jpg" height="500">
<img src="https://files.readme.io/3908492-Docs_Controls_Index_FutureFeatures.png" height="500">
<img src="https://docs.unity3d.com/2018.4/Documentation/uploads/Main/oculus_touch.jpg" height="500">

5. Assist in fitting the headset.
- [ ] Loosen straps
- [ ] Place around glasses onto head.
- [ ] Assist to tighten around-the head strap
- [ ] Assist to tighten velcro over-the-head strap.
- [ ] Check for comfort and snuggness.

6. Hand over controllers

7. Remind that they can walk around the space inside the blue boundaries.

## For introducing environments, with an exploratory purpose:

1. Make sure participants are comfortable with the controls and what they can do in the virtual space.
2. Ask questions like "what do you notice?"
3. As needed suggest, "try grabbing... what happens if you interact with... how could you...?" to lead participants towards particular functionality.

When possible avoid walking through long procedures.
