# HandWaver

- [ ] Stretching a point --> line segment --> quadrilateral
- [ ] eraser
- [ ] lines (200m line segments)
- [ ] Spatial compass and generated intersections

## Constructions
 - [ ] can you use a spatial compas to make a tetrahedron?

## Manipulation of shape
 - [ ] how do you know if two lines are parallel, skew or intersecting?
