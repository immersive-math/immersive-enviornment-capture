# Generating Composite Renderings

In simple scenarios use a Graphical video editor (Kdenlive or Davinci Resovle) to synchronize multiple feeds.
 - [ ] Use the clacker's audio and video reference points to align feeds. Keep a noise-reduction free source for this purpose.
 - [ ] Improve audio as needed, see `audio/noise_reduction` and `audio/speaker_isolation`. In particular, isolate speakers and apply noise reduction.

## Working with video captured on the Quest

We have had some problems with the encoding of time on the Quest. In particular, there are problems when audio and video are read simultaneously (as they are in Graphical video editors), even after some transformations/re-encodings. It is unclear the exact nature of this bug.

As a workaround, when using Quest video capture, we composite the video using ffmpeg and a bash script.

0. Improve audio as needed, see `audio`. In particular, isolate speakers and apply noise reduction.
1. Combine feeds with the same timestap into mkv files with ffmpeg, preserving encoding `-c:a copy -c:v copy`. This includes seperating combined video feeds into seperate streams. See example in `scripts/multiFeedMKV.sh`.
2. Generate temporary mp4 files with all audio streams (stream copies from the result of step 1). See example in `scripts/genreateMP4$.sh`.
3. Take HH:MM:SS.XXX precision measurements of the audio streams for synchronization between sources. Look for:
 - [ ] syncpoints
 - [ ] audio that is common to multiple sources (typicall room audio w/o speaker isolation is useful here)
 - [ ] start time
 - [ ] end time
4. Make notes of reference points, and calcualte the starting point for each track; and duration from start to end for the commmon timeline.
5. Generate scripts using the variables at the top of `scripts/composite_template.sh`.
 - [ ] adjust metadata as needed in outputs
6. Run these scripts to generate their output once. Since our synchronization is audio-only at this point, check the output.
 - The quest audio and video may not be synchrnoized appropriately. Count the frame difference in a video editor (e.g., Kdenlive) then ajdust the variable for the audio start time in the `composite_template`.
 - Do not apply `adelay` fitlers or `itsOffset`.
 - Note that the Quest's audio and video are seperated before being used in complex_filters where time synchronization is important. The time encoding error is unstable if audio and video are read simultaneously. By generating temporary files, we work around that.
7. Re-run the `composite_template` script. Use `CRF 0` and `veryslow` for a lossless quality recording.
8. Re-check synchronization

The continue processing with transcription and autotranscription. The `composite_template` outputs multi-channel audio to be input into the autotranscription Python methods.
