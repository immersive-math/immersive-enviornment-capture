## Extract Cropped Video with FFMPEG (multi-feed array to single feed)

To crop video, we need:
- width of crop
- height of crop
- starting x
- starting y

```
ffmpeg i video.mkv -filter:v "crop=w:h:x:y" video_singlefeed.mp4
```

To capure the bottom left 1920x1080 feed in a 4 feed array,
 - width of crop = 1920
 - height of crop = 1080
 - starting x = 0 (all the way to the left)
 - starting y = 1080 (half way down)


```
ffmpeg -i video.mkv -filter:v "crop:1920:1080:0:1080" video_singlefeed.mkv
```

As a convention, name the video `_sourceName.mkv`.


## Extract Audio Sream with FFMPEG

Audio stream are 0 indexed. The FIRST stream is track 0, SECOND stream is track 1, etc.

To extract the THIRD audio stream:
```
ffmpeg -i video.mkv -map 0:a:2 video_audioStream3.ogg
```

To extract the first and second audio streams individually:
```
ffmpeg -i video.mkv -map 0:a:0 video_audioStream1.ogg -map 0:a:1 video_audioStream2.ogg
```

Optional, `-c copy` keeps the codec intact.

As a convention, name the video `_sourceName.mkv`.

