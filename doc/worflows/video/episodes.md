# Extract Video of Episodes with FFMPEG

## Once in Bash

To extract an episode from a single or multi-track video.
 - STARTTIME: the beginning of the cut in HH:MM:SS.000
 - ENDTIME: the end of the cut in HH:MM:SS.000

 When rounding, round down startime and round up endtime.

```
ffmpeg -i video.mkv -ss STARTTIME -to ENDTIME video_episode.mkv
```

## Batches with Python
Prepare a `.csv` file with the episode names, start times and stop times.

TODO link to repository.


# Make GIF of Clips with FFMPEG (Python Library)

TODO link to repository
