# !/bin/bash

mkdir tempMP4

shopt -s nullglob
for i in *.mkv; do
	ffmpeg -y \
		-i $i \
		-map 0:a \
		-c:a copy \
		tempMP4/$i.mp4
done
