# !/bin/bash

# to find offset, use reference point with Audacity
# Make notes of measured reference points below

## Brahmagupta
# - SYNCPOINT
# - START
# - END
# - DURATION
# - Audio: PR on track ??, INTERVIEWER1 on track ??
# - Video: PR on track ??, lab on track ??
## Cantor
# - SYNCPOINT
# - START
# - PL on track ??
## Quest
# - SYNCPOINT
# - START
# - VIDEO/AUDIO OFFSET
# - INTERVIEWER2 on track 1
	


mkdir temp
mkdir ../../InterviewComposite/

#Input Specs
INPUTBRAHMAGUPTA=FILEPREFIX_brahmagupta_allFeeds.mkv
BRAHMAGUPTASTART=0:00:00.00
DURATION=0:00:00.00

INPUTCANTOR=FILEPREFIX_cantor_allFeeds.mkv
CANTORSTART=0:00:00.00

INPUTQUEST=FILEPREFIX_quest_allFeeds.mkv
QUESTAUDIOSTART=0:00:00.00
QUESTVIDEOSTART=0:00:00.00

# Output Specs
OUTPUTVIDEO=FILEPREFIX_hq.mp4
OUTPUTAUDIO=FILEPREFIX_audio.mp4

# Codec info
ABITRATE=160k
ACODEC=ac3
CRF=0
PRESET=veryslow #alternateily ultrafast

echo 'Preprocessing Brahmagupta'

ffmpeg-bar -y \
	-i $INPUTBRAHMAGUPTA \
	-ss $BRAHMAGUPTASTART \
	-t $DURATION \
	-filter_complex \
	"[0:a:4]pan=mono|c0=c0+c1,speechnorm=e=25:r=0.0001:l=1[interviewer1audio]; \
	[0:a:3]pan=mono|c0=c0+c1,speechnorm=e=25:r=0.0001:l=1[praudio]" \
	-map [praudio]:a \
	-map [interviewer1audio]:a \
	-map 0:v \
	-c:a ac3 -b:a $ABITRATE \
	-c:v copy \
	temp/.brahmagupta_trimmed.mkv

echo 'Preprocessing Cantor'

ffmpeg-bar -y  \
	-i $INPUTCANTOR \
	-ss $CANTORSTART \
	-t $DURATION \
	-filter_complex \
	"[0:a:1]pan=mono|c0=c0+c1,speechnorm=e=25:r=0.0001:l=1[plaudio]" \
	-map [plaudio]:a \
	-map 0:v \
	-c:a ac3 -b:a $ABITRATE \
	-c:v copy \
	temp/.cantor_trimmed.mkv
	
echo 'Preprocessing Quest Video - this is really slow!'

## Video ONLY
ffmpeg-bar -y  \
	-i $INPUTQUEST \
	-ss $QUESTVIDEOSTART \
	-t $DURATION \
	-filter_complex \
	"[0:v:0]scale=w=-1:h=1080,pad=1920:1080:ih:(ow-iw)/2[outvideo]" \
	-map [outvideo]:v \
	-c:v libx264 -crf $CRF -preset $PRESET -tune fastdecode \
	temp/.quest_trimmed_video.mkv
	
echo 'Preprocessing Quest Audio'
## Audio ONLY
ffmpeg-bar -y  \
	-i $INPUTQUEST \
	-ss $QUESTAUDIOSTART \
	-t $DURATION \
	-filter_complex \
	"[0:a:1]speechnorm=e=25:r=0.0001:l=1" \
	-c:a $ACODEC -b:a $ABITRATE \
	temp/.quest_trimmed_audio.mkv

echo 'Downmixing Audio'

ffmpeg-bar -y \
	-i temp/.brahmagupta_trimmed.mkv \
	-i temp/.cantor_trimmed.mkv \
	-i temp/.quest_trimmed_audio.mkv \
	-filter_complex \
	"[0:a:0][1:a:0]join=inputs=2:channel_layout=stereo[participantaudio]; \
	[0:a:1][0:a:1]join=inputs=2:channel_layout=stereo[interviewer1audio]; \
	[2:a:0][2:a:0]join=inputs=2:channel_layout=stereo[interviewer2audio]; \
	[interviewer1audio][interviewer2audio]amerge=inputs=2,pan=stereo|c0<c0+c2|c1<c1+c3[intervieweraudio]; \
	[participantaudio][intervieweraudio]amerge=inputs=2,pan=stereo|c0<c0+c2|c1<c1+c3" \
	-c:a $ACODEC -b:a $ABITRATE -ac 2\
	temp/.OUTPUT_audio_stereo.mp4

echo 'Exporting Multi-channel Audio'

ffmpeg-bar -y \
	-i temp/.brahmagupta_trimmed.mkv \
	-i temp/.cantor_trimmed.mkv \
	-i temp/.quest_trimmed_audio.mkv \
	-filter_complex \
	"[0:a:0][1:a:0][0:a:1][2:a:0]join=inputs=4:channel_layout=4.0" \
	../../InterviewComposite/$OUTPUTAUDIO

echo 'Processing Video'

ffmpeg-bar -y  \
	-i temp/.brahmagupta_trimmed.mkv \
	-i temp/.cantor_trimmed.mkv \
	-i temp/.quest_trimmed_video.mkv \
	-filter_complex \
	"[1:v:0][0:v:0][2:v:0][0:v:1]xstack=inputs=4:layout=0_0|w0_0|0_h0|w0_h0[stack]; \
	[stack]fps=30[outvideo]" \
	-map [outvideo]:v \
	-c:v libx264 -crf $CRF -preset $PRESET -tune fastdecode\
	temp/.OUTPUT_video.mp4
	
## Can add multichannel audio here.
	
echo 'Tying Things Together'		
ffmpeg-bar -y  \
	-i temp/.OUTPUT_audio_stereo.mp4 \
	-i temp/.brahmagupta_trimmed.mkv \
	-i temp/.cantor_trimmed.mkv \
	-i temp/.quest_trimmed_audio.mkv \
	-i temp/.OUTPUT_video.mp4 \
	-map 0:a \
	-metadata:s:a:0 title="MixedStereo" \
	-map 1:a \
	-metadata:s:a:1 title="ParticipantRight" \
	-metadata:s:a:3 title="Interviewer1" \
	-map 2:a \
	-metadata:s:a:2 title="ParticipantLeft" \
	-map 3:a \
	-metadata:s:a:3 title="Interviewer2" \
	-map 4:v \
	-c copy \
	../../InterviewComposite/$OUTPUTVIDEO


echo 'Cleaning Up'

rm -r temp

echo 'Done!'
