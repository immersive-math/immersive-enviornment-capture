#!/bin/bash

# Brahmagupta Group 1 and 2

ffmpeg -y  -i Brahmagupta/Group1_202211-12_14-11-53.mkv \
-filter_complex "[0:v]crop=1920:1080:1432:0[out1];[0:v]crop=1432:1080:0:0[out2]" \
-map [out1]  \
-map [out2]  \
-map 0:a  \
-metadata:s:a:0 title="PR" \
-metadata:s:a:1 title="Lab" \
-metadata:s:a:2 title="Computer" \
-c:v  ibx264 -crf 0 -preset slow -tune fastdecode -c:a copy multitrackOrigionalFootage/Group1_part1_brahmagupta_allFeeds.mkv

ffmpeg -y  -i Brahmagupta/Group1_2022-11-12_14-45-02.mkv \
-filter_complex "[0:v]crop=1920:1080:1432:0[out1];[0:v]crop=1432:1080:0:0[out2]" \
-map [out1]  \
-map [out2]  \
-map 0:a  \
-metadata:s:a:0 title="PR" \
-metadata:s:a:1 title="Lab" \
-metadata:s:a:2 title="Computer" \
-c:v  ibx264 -crf 0 -preset slow -tune fastdecode -c:a copy multitrackOrigionalFootage/Group1_part2_brahmagupta_allFeeds.mkv

ffmpeg -y  -i Brahmagupta/Group1_2022-11-12_15-10-18.mkv \
-filter_complex "[0:v]crop=1920:1080:1432:0[out1];[0:v]crop=1432:1080:0:0[out2]" \
-map [out1]  \
-map [out2]  \
-map 0:a  \
-metadata:s:a:0 title="PR" \
-metadata:s:a:1 title="Lab" \
-metadata:s:a:2 title="Computer" \
-c:v  ibx264 -crf 0 -preset slow -tune fastdecode -c:a copy multitrackOrigionalFootage/Group1_part3_brahmagupta_allFeeds.mkv


ffmpeg -y  -i Brahmagupta/Group2_2022-11-13_14-17-15.mkv \
-filter_complex "[0:v]crop=1920:1080:1432:0[out1];[0:v]crop=1432:1080:0:0[out2]" \
-map [out1]  \
-map [out2]  \
-map 0:a  \
-metadata:s:a:0 title="PR" \
-metadata:s:a:1 title="Lab" \
-metadata:s:a:2 title="Computer" \
-metadata:s:a:0 title="PR-Filtered" \
-c:v  ibx264 -crf 0 -preset slow -tune fastdecode -c:a copy multitrackOrigionalFootage/Group2_part1_brahmagupta_allFeeds.mkv

ffmpeg -y  -i Brahmagupta/Group1_2022-11-12_14-45-02.mkv \
-filter_complex "[0:v]crop=1920:1080:1432:0[out1];[0:v]crop=1432:1080:0:0[out2]" \
-map [out1]  \
-map [out2]  \
-map 0:a  \
-metadata:s:a:0 title="PR" \
-metadata:s:a:1 title="Lab" \
-metadata:s:a:2 title="Computer" \
-c:v  ibx264 -crf 0 -preset slow -tune fastdecode -c:a copy multitrackOrigionalFootage/Group2_part2_brahmagupta_allFeeds.mkv

ffmpeg -y  -i Brahmagupta/Group1_2022-11-12_15-10-18.mkv \
-filter_complex "[0:v]crop=1920:1080:1432:0[out1];[0:v]crop=1432:1080:0:0[out2]" \
-map [out1]  \
-map [out2]  \
-map 0:a  \
-metadata:s:a:0 title="PR" \
-metadata:s:a:1 title="Lab" \
-metadata:s:a:2 title="Computer" \
-c:v  ibx264 -crf 0 -preset slow -tune fastdecode -c:a copy multitrackOrigionalFootage/Group2_part3_brahmagupta_allFeeds.mkv

# Group3 Brahmagupta

ffmpeg -y  -i Brahmagupta/Group3_2023-01-10_10-43-46.mkv \
-i Brahmagupta/group3_part1_PR.ogg -i Brahmagupta/group3_part1_Camden.ogg \
-filter_complex "[0:v]crop=1920:1080:0:0[out1];[0:v]crop=1920:1080:1920:0[out2]" \
-map [out1]  \
-map [out2]  \
-map 0:a  \
-map 1:a:0  \
-map 2:a:0  \
-metadata:s:a:0 title="PR" \
-metadata:s:a:1 title="Lab" \
-metadata:s:a:2 title="Computer" \
-metadata:s:a:3 title="PR Filtered" \
-metadata:s:a:4 title="Camden" \
-c:v  ibx264 -crf 0 -preset slow -tune fastdecode -c:a copy multitrackOrigionalFootage/Group3_part1_brahmagupta_allFeeds.mkv

ffmpeg -y  -i Brahmagupta/Group3_2023-01-10_11-10-55.mkv \
-i Brahmagupta/group3_part2_camden.ogg \
-filter_complex "[0:v]crop=1920:1080:0:0[out1];[0:v]crop=1920:1080:1920:0[out2]" \
-map [out1]  \
-map [out2]  \
-map 0:a  \
-map 1:a:0  \
-metadata:s:a:0 title="PR" \
-metadata:s:a:1 title="Lab" \
-metadata:s:a:2 title="Computer" \
-metadata:s:a:3 title="Camden" \
-c:v  ibx264 -crf 0 -preset slow -tune fastdecode -c:a copy multitrackOrigionalFootage/Group3_part2_brahmagupta_allFeeds.mkv

ffmpeg -y  -i Brahmagupta/Group3_2023-01-10_11-42-16.mkv \
-i Brahmagupta/group3_part3_Camden.ogg \
-filter_complex "[0:v]crop=1920:1080:0:0[out1];[0:v]crop=1920:1080:1920:0[out2]" \
-map [out1]  \
-map [out2]  \
-map 0:a  \
-map 1:a:0  \
-metadata:s:a:0 title="PR" \
-metadata:s:a:1 title="Lab" \
-metadata:s:a:2 title="Computer" \
-metadata:s:a:3 title="Camden" \
-c:v  ibx264 -crf 0 -preset slow -tune fastdecode -c:a copy multitrackOrigionalFootage/Group3_part3_brahmagupta_allFeeds.mkv

# Cantor Group 1 and 2

ffmpeg -y  -i Cantor/Group1_2022-11-12_14-11-53.mkv \
-i Cantor/Group1_part1_camden.ogg \
-filter_complex "[0:v]crop=1920:1080:0:0[out1];[0:v]crop=1920:1080:1920:0[out2]" \
-map [out1]  \
-map [out2]  \
-map 0:a  \
-map 1:a:0  \
-metadata:s:a:0 title="PL" \
-metadata:s:a:1 title="Lab" \
-metadata:s:a:2 title="Computer" \
-metadata:s:a:3 title="Camden" \
-c:v  ibx264 -crf 0 -preset slow -tune fastdecode -c:a copy multitrackOrigionalFootage/Group1_part1_cantor_allFeeds.mkv

ffmpeg -y  -i Cantor/Group1_2022-11-12_14-45-04.mkv \
-i Cantor/Group1_part2_camden.ogg \
-filter_complex "[0:v]crop=1920:1080:0:0[out1];[0:v]crop=1920:1080:1920:0[out2]" \
-map [out1]  \
-map [out2]  \
-map 0:a  \
-map 1:a:0  \
-metadata:s:a:0 title="PL" \
-metadata:s:a:1 title="Lab" \
-metadata:s:a:2 title="Computer" \
-metadata:s:a:3 title="Camden" \
-c:v  ibx264 -crf 0 -preset slow -tune fastdecode -c:a copy multitrackOrigionalFootage/Group1_part2_cantor_allFeeds.mkv

ffmpeg -y  -i Cantor/Group1_2022-11-12_15-10-21.mkv \
-i Cantor/Group1_part3_camden.ogg \
-filter_complex "[0:v]crop=1920:1080:0:0[out1];[0:v]crop=1920:1080:1920:0[out2]" \
-map [out1]  \
-map [out2]  \
-map 0:a  \
-map 1:a:0  \
-metadata:s:a:0 title="PL" \
-metadata:s:a:1 title="Lab" \
-metadata:s:a:2 title="Computer" \
-metadata:s:a:3 title="Camden" \
-c:v  ibx264 -crf 0 -preset slow -tune fastdecode -c:a copy multitrackOrigionalFootage/Group1_part3_cantor_allFeeds.mkv

# Cantor Group2
ffmpeg -y  -i Cantor/Group2_2022-11-13_14-17-33.mkv \
-i Cantor/Group2_part1_participantL.ogg \
-i Cantor/Group2_part1_camden.ogg \ 
-filter_complex "[0:v]crop=1920:1080:0:0[out1];[0:v]crop=1920:1080:1920:0[out2]" \
-map [out1]  \
-map [out2]  \
-map 0:a  \
-map 1:a \ 
-map 2:a \ 
-metadata:s:a:0 title="PL" \
-metadata:s:a:1 title="Lab" \
-metadata:s:a:2 title="Computer" \
-metadata:s:a:3 title="PL-filtered" \
-metadata:s:a:4 title="Camden" \
-c:v  ibx264 -crf 0 -preset slow -tune fastdecode -c:a copy multitrackOrigionalFootage/Group2_part1_cantor_allFeeds.mkv

ffmpeg -y  -i Cantor/Group2_2022-11-13_14-52-03.mkv \
-i Cantor/Group2_part2_participantR.ogg \
-i Cantor/Group2_part2_camden.ogg \ 
-filter_complex "[0:v]crop=1920:1080:0:0[out1];[0:v]crop=1920:1080:1920:0[out2]" \
-map [out1]  \
-map [out2]  \
-map 0:a  \
-metadata:s:a:0 title="PL" \
-metadata:s:a:1 title="Lab" \
-metadata:s:a:2 title="Computer" \
-metadata:s:a:3 title="PR-filtered" \
-metadata:s:a:4 title="Camden" \
-c:v  ibx264 -crf 0 -preset slow -tune fastdecode -c:a copy multitrackOrigionalFootage/Group2_part2_cantor_allFeeds.mkv

ffmpeg -y  -i Cantor/Group2_2022-11-13_15-17-29.mkv \
-i Cantor/Group2_part3_participantR.ogg \
-i Cantor/Group2_part3_camden.ogg \ 
-filter_complex "[0:v]crop=1920:1080:0:0[out1];[0:v]crop=1920:1080:1920:0[out2]" \
-map [out1]  \
-map [out2]  \
-map 0:a  \
-metadata:s:a:0 title="PL" \
-metadata:s:a:1 title="Lab" \
-metadata:s:a:2 title="Computer" \
-metadata:s:a:3 title="PR-filtered" \
-metadata:s:a:4 title="Camden" \
-c:v  ibx264 -crf 0 -preset slow -tune fastdecode -c:a copy multitrackOrigionalFootage/Group2_part3_cantor_allFeeds.mkv

# Group3 Cantor

ffmpeg -y  -i Cantor/Group3_2023-01-10_10-43-48.mkv \
-filter_complex "[0:v]crop=1920:1080:0:0[out1];[0:v]crop=1920:1080:1920:0[out2]" \
-map [out1]  \
-map [out2]  \
-map 0:a  \
-metadata:s:a:0 title="PL" \
-metadata:s:a:1 title="Lab" \
-metadata:s:a:2 title="Computer" \
-c:v  ibx264 -crf 0 -preset slow -tune fastdecode -c:a copy multitrackOrigionalFootage/Group3_part1_cantor_allFeeds.mkv

ffmpeg -y  -i Cantor/Group3_2023-01-10_11-10-59.mkv \
-filter_complex "[0:v]crop=1920:1080:0:0[out1];[0:v]crop=1920:1080:1920:0[out2]" \
-map [out1]  \
-map [out2]  \
-map 0:a  \
-metadata:s:a:0 title="PL" \
-metadata:s:a:1 title="Lab" \
-metadata:s:a:2 title="Computer" \
-c:v  ibx264 -crf 0 -preset slow -tune fastdecode -c:a copy multitrackOrigionalFootage/Group3_part2_cantor_allFeeds.mkv

ffmpeg -y  -i Cantor/Group3_2023-01-10_11-42-19.mkv \
-filter_complex "[0:v]crop=1920:1080:0:0[out1];[0:v]crop=1920:1080:1920:0[out2]" \
-map [out1]  \
-map [out2]  \
-map 0:a  \
-metadata:s:a:0 title="PL" \
-metadata:s:a:1 title="Lab" \
-metadata:s:a:2 title="Computer" \
-c:v  ibx264 -crf 0 -preset slow -tune fastdecode -c:a copy multitrackOrigionalFootage/Group3_part3_cantor_allFeeds.mkv

# Quest Group 1

ffmpeg -y  -i Quest/Group1_com.oculus.browser-20221112-111142.mp4 \
 -i Quest/group1_part1_justin.ogg \
-map 0 -map 1:a:0 \
-metadata:s:a:0 title="Quest" \
-metadata:s:a:1 title="Justin" \
-c:v  ibx264 -crf 0 -preset slow -tune fastdecode -c:a copy multitrackOrigionalFootage/Group1_part1_quest_allFeeds.mkv

ffmpeg -y  -i Quest/Group1_com.oculus.browser-20221112-114541.mp4 \
-i Quest/group1_part2_justin.ogg \
-map 0 -map 1:a:0 \
-metadata:s:a:0 title="Quest" \
-metadata:s:a:1 title="Justin" \
-c:v  ibx264 -crf 0 -preset slow -tune fastdecode -c:a copy multitrackOrigionalFootage/Group1_part2_quest_allFeeds.mkv

ffmpeg -y  -i Quest/Group1_com.oculus.browser-20221112-122734.mp4 \
-i Quest/group1_part3_justin.ogg \
-map 0 -map 1:a:0 \
-metadata:s:a:0 title="Quest" \
-metadata:s:a:1 title="Justin" \
-c:v  ibx264 -crf 0 -preset slow -tune fastdecode -c:a copy multitrackOrigionalFootage/Group1_part3_quest_allFeeds.mkv

ffmpeg -y  -i Quest/Group1_com.oculus.shellenv-20221112-121150.mp4 \
-i Quest/group1_part3a_justin.ogg \
-map 0 -map 1:a:0 \
-metadata:s:a:0 title="Quest" \
-metadata:s:a:1 title="Justin" \
-c:v  ibx264 -crf 0 -preset slow -tune fastdecode -c:a copy multitrackOrigionalFootage/Group1_part3a_quest_allFeeds.mkv

# Quest Group 2

ffmpeg -y  -i Quest/Group2_com.oculus.browser-20221113-111339.mp4 \
-i Quest/group2_part1_justin.ogg \
-map 0 -map 1:a:0 \
-metadata:s:a:0 title="Quest" \
-metadata:s:a:1 title="Justin" \
-c:v  ibx264 -crf 0 -preset slow -tune fastdecode -c:a copy multitrackOrigionalFootage/Group2_part1_quest_allFeeds.mkv

ffmpeg -y  -i Quest/Group2_com.oculus.browser-20221113-115112.mp4 \
-i Quest/group2_part2_justin.ogg \
-map 0 -map 1:a:0 \
-metadata:s:a:0 title="Quest" \
-metadata:s:a:1 title="Justin" \
-c:v  ibx264 -crf 0 -preset slow -tune fastdecode -c:a copy multitrackOrigionalFootage/Group2_part2_quest_allFeeds.mkv

ffmpeg -y  -i Quest/Group2_com.oculus.browser-20221113-121825.mp4 \
-i Quest/group2_part3_justin.ogg \
-map 0 -map 1:a:0 \
-metadata:s:a:0 title="Quest" \
-metadata:s:a:1 title="Justin" \
-c:v  ibx264 -crf 0 -preset slow -tune fastdecode -c:a copy multitrackOrigionalFootage/Group2_part3_quest_allFeeds.mkv

# Group3 Quest

ffmpeg -y  -i Quest/January23com.oculus.browser-20230110-073647.mp4 \
-i Quest/January23interview3_justin.ogg \
-map 0 -map 1:a:0 \
-c:v  ibx264 -crf 0 -preset slow -tune fastdecode -c:a copy multitrackOrigionalFootage/Gorup3_part1_quest_allFeeds.mkv


ffmpeg -y  -i Quest/January23com.oculus.shellenv-20230110-081115.mp4 \
-i Quest/January23interview3a_quest_JUSTIN.ogg \
-map 0 -map 1:a:0 \
-c:v  ibx264 -crf 0 -preset slow -tune fastdecode -c:a copy multitrackOrigionalFootage/Gorup3_part2a_quest_allFeeds.mkv

ffmpeg -y  -i Quest/January23com.oculus.shellenv-20230110-082000.mp4 \
-i Quest/January23interview3b_quest_JUSTIN.ogg \
-map 0 -map 1:a:0 \
-c:v  ibx264 -crf 0 -preset slow -tune fastdecode -c:a copy multitrackOrigionalFootage/Gorup3_part2b_quest_allFeeds.mkv

ffmpeg -y  -i Quest/January23com.oculus.shellenv-20230110-084135.mp4 \
-i Quest/January23interview3c_quest_JUSTIN.ogg \
-map 0 -map 1:a:0 \
-c:v  ibx264 -crf 0 -preset slow -tune fastdecode -c:a copy multitrackOrigionalFootage/Gorup3_part3_quest_allFeeds.mkv
