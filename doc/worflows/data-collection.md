# Data Collection

## Configuration and Prep
1. Configure [OBS For Screen, Camera and Microphone Capture](capture/obs/configuration).
2. Complete the [pre-recording checklist](capture/precheck).

## Interview or Observation
3. Record an interview or observation session.
4. Follow [file management](capture/file-management) to ensure redundant copies are made. This is a verification of an automated step.

## Pre-Processing
5. [Seperate each video and audo stream](video/audio-video-extraction), naming by their source. Do not cut the audio or video recordings.
6. Follow [file management](capture/file-management) to ensure redundant copies are made. This is a verification of an automated step.
7. [Isolate speaker audio](audio/isolateSpeaker) for each speaker. This may include working with multiple speakers from a single stream, or multiple streams for a single speaker.
8. [Apply noise reduction](audio/noiseReduction) for each speaker's audio, as appropritate. Do not cut the audio recording.
9. [Composite](video/composite) video and audio streams, with synchronization. Audio streams from the multi-stream origional recording should serve as the primary record of synchronization. Cut the composite recording as appropriate.
10. Render the projecct developed in Step 9.
10. [Seperate the audio stream](video/audio-video-extraction) from the composite recording from step 10.

## Transcript Development
11. [Auto-transcribe](transcription/local-auto-transcription) with Whisper, using the .ogg track from the composite, synchronized.
12. [Manually review](transcription/manual-transcript-correction) Whisper's automatic transcription, identifying speakers, correcting words, correcting timings, and implementing conventions.
13. [Reformat transcript](transcription/manual-transcript-correction) for readability. This is an optional step.
14. [Embed subtitles](transcription/embedded-subtitles) in the composite rendering from step 10, saving as a new archive copy.
15. [Bake subtitles](transcription/baked-subtitles) in the composite rendering from step 10, saving as a new archive copy.

## Episode Selection
16. Make a CSV with episode names and timings for each rendering from step 10. [Cut episodes](video/episodes) in batches, with embedded subtitles.
17. Optional. [Cut episodes](video/episodes) in batches, with baked subtitles. Baked subtitles are helpful when working with Anotemos' local file option.

## Multi-modal Transcripts
!! TODO !!

## Archives
 - The results of steps 14 and/or 15 are primary archives of the data

### Interpretive Archives
 - Additional archives may be produced with muli-modal transcripts, using reveal.js, overlays, or other formats.
 - Additional archives may also be derived by editing the KDenlive project from Step 9. (i.e., to highlight features in the video)
