Check out a model of our lab here!

[IMRE Lab in AFrame](https://immersive-math.gitlab.io/vr-classroom-labratory-model/)

# Archimedes

Archimedes has three primary purposes and one secondary purpose

- editing of video data

- analysis of video data

- production quality graphics

- linux development and testing

## Hardware Configuration

- Ryzen 5th Gen R7

- NVidia GTX 1080

- 32 GB RAM


# Poincare

Poincare's primary use is network attached storage. All of Poincare's storage is redundant with Raid. Poincare holds both backups and working copies of all IMRE Lab development and research work. GitLab and Google Drive are used for cloud backups of many projects.

## Hardware Configuration

- Ryzen 3rd Gen R7

- NVidia GTX 1080

- 32 GB RAM

# Brahmagupta & Cantor

Brahmagupta and Cantor are identical setups for Wireless Vive Pro and Vive Pro use on Windows 11.

## Hardware

- Wireless Vive Adapter

- Ryzen 1700X

- AMD Radeon RX 6900XT

- 32 GB RAM

- Vive Pro or Vive Pro 2

- Valve Index Controllers or Vive Pro Controllers

# Legacy Equipment

## Leap Motion

## HTC Vive
