[![Univesity of Maine Student Symposium 23](https://umaine.edu/umss/wp-content/uploads/sites/458/2022/06/Student-Symposium-logo-no-date-2000px-wide-1268x506.png)](https://umaine.edu/umss/)

# Multi-Hand Collaborative Digital Spatial Painting and Embodiment in Geometry Diagrams

**[<img src=https://umaine.edu/imre/wp-content/themes/umaine/content-blocks/people-list/silhouette.png height=50> Brooke Howlett](https://umaine.edu/imre/people/brooke-howlett/)**<sup>[1,2](#author-affiliations)</sup>; **[<img src=https://umaine.edu/imre/wp-content/themes/umaine/content-blocks/people-list/silhouette.png height = 50> Joshua Bohm](https://umaine.edu/imre/people/joshua-bohm/)**<sup>[1,3](#author-affiliations)</sup>; **[<img src=https://umaine.edu/imre/wp-content/uploads/sites/286/2016/12/00100lPORTRAIT_00100_BURST20190928134708418_COVER-423x564.jpg height=50> Camden Bock](https://umaine.edu/imre/people/camden-bock/)**<sup>[1,2,4](#author-affiliations)</sup>; [<img src=https://umaine.edu/edhd/wp-content/uploads/sites/54/2019/01/Justin-Dimmel-200x300.jpg height=50> Justin Dimmel](https://umaine.edu/edhd/facultystaff/justin-dimmel/) <sup>[1,2,4](#author-affiliations)</sup>, Ph.D. 

[![PosterGraphic](https://gitlab.com/immersive-math/immersive-math.gitlab.io/-/raw/main/presentations/umss23/img/UMSS23_Poster.svg)](https://gitlab.com/immersive-math/immersive-math.gitlab.io/-/raw/main/presentations/umss23/img/UMSS23_Poster.pdf)

## Abstract
In school geometry, diagrams are often inscribed on two-dimensional surfaces like paper, whiteboards and computer screens. With immersive virtual reality environments, mathematical diagrams can be digitally rendered in three-dimensions at human-scale. 
Digital spatial painting tools can support learners' interactions with these diagrams simultaneously with multiple hands, including with three or more hands in collaborative settings. 
We report on a study where learners worked in pairs to coordinate their use of multi-hand spatial painting tools to make mathematical diagrams. In semi-structured interviews, participants had access to a collaborative spatial painting environment where they could use a multi-hand painting tool for a total of one hour. 
Participants were given prompts similar to “how many ways can you make a cylinder.” 
We describe how learners’ inscriptions embodied mathematical relationships and allowed learners to center themselves within diagrams, including solids and surfaces of revolution. 
Our analysis suggests that the design of spatial diagramming environments should consider the opportunities for embodied connections afforded by large-scale, collaborative, and multi-handed interactions. 
Our exhibit includes a demonstration of the virtual spatial painting environment.

## Purpose

The purpose of this research is to understand how a **new tool** can be used to support students' **learning** about geometry and mathematics more broadly.
Immersive spatial diagrams are an emerging educational technology (Kaufmann & Schmalstieg, 2006; Cangas et al., 2019; Dimmel & Bock, 2019; Dimmel et al., 2020), which offers tangible access to point-of-view (Bock ^& Dimmel, 2021a), unbounded renderings (Bock & Dimmel, 2021b) and embodied experiences (Walkington et al., 2022; Walkington ) of diagrams.
Research on collaborative uses of immersive spatial diagrams is limited, usually documented a-symmetric access to diagrams  where only one learner can interact with the diagram at a time (Rodriguez et al., 2021; Price et al., 2020;  Flatland XR Channel, 2021).
We are particularly interested in supporting learners' active learning that encourages strong and novel connections between concepts, through tangible representations.

## Research Question

How does learners' collaboration change their process of developing understandings of geometric relationships?

## Conceptual Framework: Diagrams
 - Learners often use diagrams to build understandings of geometric figures.
 - Diagrams have many potentials, which are supported by the inscription technology
 - Learners realize potentials of diagrams through discourse that interacts with the diagram. (Sinclair et al., 2013)
 - These realized potentials shape learners' understanding of geometric figures.

![Conceptual Framework](https://gitlab.com/immersive-math/immersive-math.gitlab.io/-/raw/main/presentations/umss23/img/concept_drawing.svg)

 - Collaborative virtual relaity can support novel interactions with geometric figures. (Rodriguez et al., 2021; Price et al., 2020;Flatland XR Channel, 2021)

## Collaborative Multi-Hand Spatial Painting Enviornment

[Try the WebXR Environment Here!](https://imrelabpoincare.umeedu.maine.edu:9080/a-painter_noaudio.html)
(Mozilla, 2017; Knispel & Bullock, 2017; Lee, 2018; Bock, 2022)

 - Immersive 6-DOF virtual Reality
 - Spatial Painting Tool, traces curves or surfaces
 - Brushes connect multiple hands, across multiple learners

<div style="max-height=1000px"><div style="position:relative;padding-bottom:125.82236842105%"><iframe id="kaltura_player" src="https://cdnapisec.kaltura.com/p/2189801/sp/218980100/embedIframeJs/uiconf_id/44739341/partner_id/2189801?iframeembed=true&playerId=kaltura_player&entry_id=1_6xwssqs9&flashvars[streamerType]=auto&amp;flashvars[localizationCode]=en&amp;flashvars[leadWithHTML5]=true&amp;flashvars[sideBarContainer.plugin]=true&amp;flashvars[sideBarContainer.position]=left&amp;flashvars[sideBarContainer.clickToClose]=true&amp;flashvars[chapters.plugin]=true&amp;flashvars[chapters.layout]=vertical&amp;flashvars[chapters.thumbnailRotator]=false&amp;flashvars[streamSelector.plugin]=true&amp;flashvars[EmbedPlayer.SpinnerTarget]=videoHolder&amp;flashvars[dualScreen.plugin]=true&amp;flashvars[hotspots.plugin]=1&amp;flashvars[Kaltura.addCrossoriginToIframe]=true&amp;&wid=1_qcevo5lx" width="608" height="765" allowfullscreen webkitallowfullscreen mozAllowFullScreen allow="autoplay *; fullscreen *; encrypted-media *" sandbox="allow-downloads allow-forms allow-same-origin allow-scripts allow-top-navigation allow-pointer-lock allow-popups allow-modals allow-orientation-lock allow-popups-to-escape-sandbox allow-presentation allow-top-navigation-by-user-activation" frameborder="0" title="A-Painter Geometry Sketches" style="position:absolute;top:0;left:0;width:100%;height:100%"></iframe></div></div>

[![GitLab Logo](https://about.gitlab.com/images/press/logo/svg/gitlab-logo-100.svg)](https://gitlab.com/immersive-math/naf-a-painter)

## Methodology

### Semi-structured task-based interviews
 - Pairs of university students
 - Three 20-minute sessions in collaborative, immersive virtual relaity
 - Tasks: "How many ways can you make _____?"

### Data Sources
 - First-person virtual video recording for each participant and immersed interviewer
 - Third-person physical video recording of labratory classroom
 - Audio recording of dialouge
 - Multi-modal transcription

![Data Sources](https://gitlab.com/immersive-math/immersive-math.gitlab.io/-/raw/main/presentations/umss23/img/data_capture.svg)

### Coding

- 120 episodes of attempted inscriptions of geometric figures
- Axial coding for each episode:
    1. Roles of participants and interviewer
    2. Which resources were used
    3. How did the learner(s) coordinate their inscription

## Results

![Episode 2101](https://gitlab.com/immersive-math/immersive-math.gitlab.io/-/raw/main/presentations/umss23/img/2101_Square1.gif)

![Episode 3101](https://gitlab.com/immersive-math/immersive-math.gitlab.io/-/raw/main/presentations/umss23/img/3101_LineSegment.gif)

![Episode 3117](https://gitlab.com/immersive-math/immersive-math.gitlab.io/-/raw/main/presentations/umss23/img/3117_Circle7.gif)

![Episdoe 3122](https://gitlab.com/immersive-math/immersive-math.gitlab.io/-/raw/main/presentations/umss23/img/3122_Cylinder3.gif)

![Episdoe 3126](https://gitlab.com/immersive-math/immersive-math.gitlab.io/-/raw/main/presentations/umss23/img/3126_Triangularprism.gif)

## Discussion
Collaborative drawings used multi-body resources to embody mathematical relationships.
 - Use of gaze as an axis
 - Inscribing around another learner's body
 - Inscribing around the projection of another learner's head

## Implications

### Mathematics Education Research

Our research extends what is known about the affordances of immersive spatial diagrams, with specific attention to collaboration.
Simultaneous, collaborative construction of diagrams with embodied co-ordination is under-researched in mathematics education, but has been documented in functions settings (York et al., 2022).

The participants in this study were adult university students, and many of which had moderate to strong backgrounds in working with mathematical concepts. With these participants, we observed how they conceptualized abstract mathematical concepts. In our analysis, we attended to how the ability to collaborate within the virtual environment aided and affected that conceptualization. 

For the use of this tool in future education, it would be incredibly beneficial and interesting to allow groups that aren’t already proficient in mathematics to use this tool. It would provide an even larger opportunity to observe how VR can support entirely new connections, rather than see how it affects connections to familiar concepts. Moving forward with this research we would like to involve students within the elementary and secondary education systems, as well as undergraduate college students who are enrolled in mathematics courses. For example, we are excited about applications to:
 - Elementary classifications of shapes
 - Measurement in secondary geometry 
 - Surfaces and Volumes of Revolution in calculus 2 classes

	

### Teaching of Mathematics (K-16)

This research has exciting applications to the teaching of mathematics. Teachers should consider how learners' might experience diagrams from inside the diagram (Benally et al., 2022), and new modes for learners to collaborate while making or interacting with diagrams (Chorney & Sinclair, 2018). These could offer new ways for students to communicate their understanding of mathematical concepts.

## References

 - Bock, C. G. (2022). Immersive-math/naf-a-painter (1e02bad0b16fb5890cd22f2a60f22f142d6183b5) [JavaScript]. The University of Maine. https://gitlab.com/immersive-math/naf-a-painter
 - Bock, C. G., & Dimmel, J. K. (2021a). Dynamic Spatial Diagrams and Solid Geometric Figures. Psychology of Mathematics Education North America.
 - Bock, C. G., & Dimmel, J. K. (2021b). Digital Representations without Physical Analogues: A Study of Body-Based Interactions with an Apparently Unbounded Spatial Diagram. Digital Experiences in Mathematics Education, 7(2), 193–221. https://doi.org/10.1007/s40751-020-00082-4
 - Cangas, D., Morga, G., & Rodríguez, J. L. (2019). Geometry teaching experience in virtual reality with NeoTrie VR. Psychology, Society, & Education, 11(3), 355–366. https://www.researchgate.net/deref/http%3A%2F%2Fdx.doi.org%2F10.25115%2Fpsye.v11i3.2270?_sg%5B0%5D=v8lGDwRWBbA_jeBljwsTMB0eVZZgSEze1y9GIcH3uooarxwnQ90EyoNFvF5f7SEGBIHLh_AQL_S7kxRX4n1Fp0p-eg.gYx91pZm6JBMMBFulo2UxmW_jFC205iyuUMZL571ELu9Dwji_2G8F23yZlSzFdKgNgCk4ZBxVVSSmI0l3Jgf7A
 - Chorney, S., & Sinclair, N. (2018). Fingers-on geometry: The emergence of symmetry in a primary school classroom with multi-touch dynamic geometry. Using Mobile Technologies in the Teaching and Learning of Mathematics, 213–230.

 - Dimmel, J., & Bock, C. (2019). Dynamic Mathematical Figures with Immersive Spatial Displays: The Case of Handwaver. In G. Aldon & J. Trgalova (Eds.), Technology in Mathematics Teaching: Selected Papers of the 13th ICTMT Conference (pp. 99–122). Springer International Publishing. https://doi.org/10.1007/978-3-030-19741-4_5
 - Dimmel, J., Pandiscio, E., & Bock, C. (2020). The Geometry of Movement: Encounters with Spatial Inscriptions for Making and Exploring Mathematical Figures. Digital Experiences in Mathematics Education. https://doi.org/10.1007/s40751-020-00078-0
 - Flatland XR Channel (Director). (2021, December 13). AR Collaboration. https://www.youtube.com/watch?v=HFdOBiHzZes
 - Kaufmann, H., & Schmalstieg, D. (2006). Designing Immersive Virtual Reality for Geometry Education. IEEE Virtual Reality Conference (VR 2006), 51–58. https://doi.org/10.1109/VR.2006.48
 - Knispel, J., & Bullock, F. (2017). Collaborative VR painting in web browsers. SIGGRAPH Asia 2017 VR Showcase, 1–2. https://doi.org/10.1145/3139468.3148451
 - Lee, H. (2018). Haydenjameslee/networked-a-painter (a58aec460128d5902aaedac3ef68799c2fdf397b) [JavaScript]. https://github.com/haydenjameslee/networked-a-painter
 - Mozilla. (2017). Aframevr/a-painter [JavaScript]. A-Frame. https://github.com/aframevr/a-painter (Original work published 2016)
 - Price, S., Yiannoutsou, N., & Vezzoli, Y. (2020). Making the Body Tangible: Elementary Geometry Learning through VR. Digital Experiences in Mathematics Education. https://doi.org/10.1007/s40751-020-00071-7
 - Rendever. (2022). MultiBrush on Oculus Quest. https://www.oculus.com/experiences/quest/3438333449611263/
 - Sinclair, N., Freitas, E., & Ferrara, F. (2013). Virtual encounters: The murky and furtive world of mathematical inventiveness. ZDM, 45(2), 239–252. https://doi.org/10.1007/s11858-012-0465-3
 - Rodríguez, J. L., Romero, I., & Codina, A. (2021). The Influence of NeoTrie VR’s Immersive Virtual Reality on the Teaching and Learning of Geometry. Mathematics, 9(19), Article 19. https://doi.org/10.3390/math9192411
 - Walkington, C., Nathan, M. J., Hunnicutt, J., Washington, J., & Holcomb-Webb, K. (2022). Learning geometry through collaborative, embodied explorations with augmented reality holograms. Proceedings of the 16th International Conference of the Learning Sciences–ICLS, 1992–1993.
 - York, T., Greenstein, S., & Akoum, D. (2022). Embodying covariation through collaborative instrumentation. In A. F. Lischka, E. B. Dyer, R. S. Jones, J. N. Lovett, J. Strayer, & S. Drown (Eds.), Proceedings of the forty-fourth annual meeting of the North American Chapter of the International Group for the Psychology of Mathematics Education. Middle Tennessee State University.

## Author Affiliations
*The University of Maine*
1. Immersive Mathematics in Rendered Enviornments Labratory
2. School of Learning and Teaching
3. Ecology and Environmental Science Program
4. Maine Center for Research in STEM Education

<!-- footer -->
Poster Design Method: [#BetterPoster](https://osf.io/ef53g/)