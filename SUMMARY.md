# SUMMARY

* [Introduction](README.md)
    * [University Homepage](https://umaine.edu/imre/)
* Projects
    * [Networked A-Painter Diagrams](https://imrelabpoincare.umeedu.maine.edu:9080/a-painter_noaudio.html)
        * [Shearing of Triangle](https://imrelabpoincare.umeedu.maine.edu:9080/shearing_triangle_noaudio.html)
        * [Shearing of Pyramid](https://imrelabpoincare.umeedu.maine.edu:9080/shearing_pyramid_noaudio.html)
    * [HandWaver 2019](https://gitlab.com/immersive-math/handwaver/-/releases/HW19.05_Lite)
* Presentations
    * [UMSS23](presentations/umss23/digitalposter.md)
* Lab Operations
    * Workflows
        * [Data Collection](doc/worflows/data-collection.md)
        * Demonstrations
            * [A-Painter](doc/worflows/demos/a-painter.md)
            * [HandWaver](doc/worflows/demos/handwaver.md)
        * Capture
            * [OBS Configuration](doc/worflows/capture/obs/configuration.md)
            * [File Management](doc/worflows/capture/file-management.md)
            * [Pre-Checks](doc/worflows/capture/precheck.md)
        * Audio
            * [Isolating Speakers](doc/worflows/audio/isolateSpeaker.md)
            * [Noise Reduction](doc/worflows/audio/noiseReduction.md)
        * Video
            * [Archives](doc/worflows/video/archives.md)
            * [Audio and Video Extraction](doc/worflows/video/audio-video-extraction.md)
            * [Composites](doc/worflows/video/compsite.md)
            * [Episodes](doc/worflows/video/episodes.md)
        * [Software](doc/worflows/software/history.md)
            * [Project Management](doc/worflows/software/projectManagement.md)
            * [Git](doc/worflows/software/git.md)
            * [GitLab](doc/worflows/software/gitlab.md)
            * [A-Frame](doc/worflows/software/aframe-development.md)
            * [WebXR](doc/worflows/software/webXR.md)
            * [Unity](doc/worflows/software/unity-development.md)
        * Transcription
            * [Local AutoTranscription](doc/worflows/transcription/local-autotranscription.md)
            * [Manual Transcription Correction](doc/worflows/transcription/manual-transcript-correction.md)
            * [Baked Subtitles](doc/worflows/transcription/baked-subtitles.md)
            * [Embedded Subtitles](doc/worflows/transcription/embedded-subtitles.md)
        * Misc
            <!--* [UMCOEHD Poster Printing](doc/worflows/misc/posters/COEHD_PosterPrinting.pdf)-->
            * [Book Scanning](doc/worflows/misc/book_scan.md)
    * [Lab Equipment](doc/lab/equipment-summary.md)
        * [VR Model of Labratory Classroom](https://immersive-math.gitlab.io/vr-classroom-labratory-model/)
        * [Audio & Video](doc/equipment/audio-video.md)
        * Head Mounted Displays
            * [Quest](doc/equipment/hmd/quest.md)
            * [Vive Pro](doc/equipment/hmd/vive-pro.md)
        * Workstations
            * [Archimedes](doc/equipment/workstations/archimedes.md)
            * [Brahmagupta](doc/equipment/workstations/brahmagupta.md)
            * [Cantor](doc/equipment/workstations/cantor.md)
        * [Poincare Server](doc/equipment/server/poincare.md)
        * [Maintenance](doc/equipment/maintenance.md)